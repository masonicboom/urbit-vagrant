To get started with [Urbit](http://www.urbit.org):

  1. Install [Vagrant](http://www.vagrantup.com).
  1. Install [VirtualBox](https://www.virtualbox.org).
  1. Clone this repository.
  1. In a terminal within the cloned folder, execute "vagrant up".
  1. Follow the "NEXT STEPS" it gives you at the end.

For more details, see [http://www.urbit.org/2013/11/18/urbit-is-easy-ch1.html](http://www.urbit.org/2013/11/18/urbit-is-easy-ch1.html).
