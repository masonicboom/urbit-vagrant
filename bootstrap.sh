#!/usr/bin/env bash

USER=vagrant

apt-get update
apt-get install -y libgmp3-dev libsigsegv-dev openssl libssl-dev libncurses5-dev git make exuberant-ctags

git clone https://github.com/urbit/urbit.git
pushd urbit
make
popd
chown --recursive $USER:$USER urbit

echo "export URBIT_HOME=/home/$USER/urbit/urb" >> /home/$USER/.bash_profile

echo "NEXT STEPS:"
echo "  1) vagrant ssh"
echo "  2) \$URBIT_HOME/../bin/vere -c mypier"
